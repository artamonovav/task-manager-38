package ru.t1.artamonov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.artamonov.tm.api.repository.IProjectRepository;
import ru.t1.artamonov.tm.api.service.IConnectionService;
import ru.t1.artamonov.tm.marker.UnitCategory;
import ru.t1.artamonov.tm.service.ConnectionService;
import ru.t1.artamonov.tm.service.PropertyService;

import java.sql.Connection;

import static ru.t1.artamonov.tm.constant.ProjectTestData.*;
import static ru.t1.artamonov.tm.constant.UserTestData.USER1;
import static ru.t1.artamonov.tm.constant.UserTestData.USER2;

@Category(UnitCategory.class)
public final class ProjectRepositoryTest {

    @NotNull
    private final PropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final Connection connection = connectionService.getConnection();

    @NotNull
    IProjectRepository projectRepository = new ProjectRepository(connection);

    @After
    @SneakyThrows
    public void dropConnection() {
        connection.rollback();
        connection.close();
    }

    @Before
    public void init() {
        projectRepository.clear();
    }

    @Test
    public void add() {
        Assert.assertTrue(projectRepository.findAll().isEmpty());
        projectRepository.add(USER1_PROJECT1);
        projectRepository.findOneById(USER1_PROJECT1.getId());
        Assert.assertEquals(USER1_PROJECT1.getId(), projectRepository.findAll().get(0).getId());
    }

    @Test
    public void addByUserId() {
        Assert.assertTrue(projectRepository.findAll().isEmpty());
        projectRepository.add(USER1.getId(), USER1_PROJECT1);
        Assert.assertEquals(USER1_PROJECT1.getId(), projectRepository.findAll().get(0).getId());
        Assert.assertEquals(USER1.getId(), projectRepository.findAll().get(0).getUserId());
    }

    @Test
    public void clearByUserId() {
        Assert.assertTrue(projectRepository.findAll().isEmpty());
        projectRepository.add(USER1_PROJECT_LIST);
        Assert.assertEquals(USER1_PROJECT_LIST.size(), projectRepository.findAll().size());
        projectRepository.clear(USER2.getId());
        Assert.assertFalse(projectRepository.findAll().isEmpty());
        projectRepository.clear(USER1.getId());
        Assert.assertTrue(projectRepository.findAll().isEmpty());
        projectRepository.add(USER2_PROJECT1);
        projectRepository.clear(USER1.getId());
        Assert.assertFalse(projectRepository.findAll().isEmpty());
    }

    @Test
    public void findAllByUserId() {
        Assert.assertTrue(projectRepository.findAll().isEmpty());
        projectRepository.add(PROJECT_LIST);
        Assert.assertEquals(USER1_PROJECT_LIST.size(), projectRepository.findAll(USER1.getId()).size());
    }

    @Test
    public void findOneByIdByUserId() {
        Assert.assertTrue(projectRepository.findAll().isEmpty());
        projectRepository.add(USER1_PROJECT1);
        projectRepository.add(USER2_PROJECT1);
        Assert.assertEquals(USER1_PROJECT1.getId(), projectRepository.findOneById(USER1.getId(), USER1_PROJECT1.getId()).getId());
    }

    @Test
    public void removeByUserId() {
        Assert.assertTrue(projectRepository.findAll().isEmpty());
        projectRepository.add(USER1_PROJECT1);
        projectRepository.add(USER2_PROJECT1);
        Assert.assertEquals(USER1_PROJECT1, projectRepository.remove(USER1.getId(), USER1_PROJECT1));
    }

    @Test
    public void removeByIdByUserId() {
        Assert.assertTrue(projectRepository.findAll().isEmpty());
        projectRepository.add(USER1_PROJECT1);
        projectRepository.add(USER2_PROJECT1);
        Assert.assertEquals(USER1_PROJECT1.getId(), projectRepository.removeById(USER1.getId(), USER1_PROJECT1.getId()).getId());
    }

    @Test
    public void existsByIdByUserId() {
        Assert.assertTrue(projectRepository.findAll().isEmpty());
        projectRepository.add(USER1_PROJECT1);
        Assert.assertTrue(projectRepository.existsById(USER1_PROJECT1.getId()));
        Assert.assertFalse(projectRepository.existsById(USER2_PROJECT1.getId()));
    }

}
