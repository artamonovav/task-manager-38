package ru.t1.artamonov.tm.api.repository;

import org.jetbrains.annotations.Nullable;
import ru.t1.artamonov.tm.model.User;

public interface IUserRepository extends IRepository<User> {

    @Nullable
    User findByLogin(@Nullable String login);

    @Nullable
    User findByEmail(@Nullable String email);

}
