package ru.t1.artamonov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.artamonov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.artamonov.tm.api.endpoint.IProjectEndpoint;
import ru.t1.artamonov.tm.dto.request.*;
import ru.t1.artamonov.tm.dto.response.*;
import ru.t1.artamonov.tm.enumerated.Status;
import ru.t1.artamonov.tm.marker.IntegrationCategory;

import java.util.UUID;

@Category(IntegrationCategory.class)
public final class ProjectEndpointTest {

    @NotNull
    private final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance();

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance();

    @Nullable
    private String userToken;

    @Before
    public void init() {
        @Nullable final UserLoginResponse loginResponse = authEndpoint.login(
                new UserLoginRequest("test", "test")
        );
        userToken = loginResponse.getToken();
    }

    @After
    public void clear() {
        projectEndpoint.clearProject(new ProjectClearRequest(userToken));
    }

    @Test
    public void projectChangeStatusById() {
        @NotNull final String testProjectName = "UnitTestProject_" + UUID.randomUUID().toString();
        Assert.assertThrows(
                Exception.class, () -> projectEndpoint.changeProjectStatusById(
                        new ProjectChangeStatusByIdRequest())
        );
        Assert.assertThrows(
                Exception.class, () -> projectEndpoint.changeProjectStatusById(
                        new ProjectChangeStatusByIdRequest(null, null, null))
        );
        Assert.assertThrows(
                Exception.class, () -> projectEndpoint.changeProjectStatusById(
                        new ProjectChangeStatusByIdRequest("wrongToken", null, null))
        );
        Assert.assertThrows(
                Exception.class, () -> projectEndpoint.changeProjectStatusById(
                        new ProjectChangeStatusByIdRequest(userToken, null, null))
        );
        Assert.assertThrows(
                Exception.class, () -> projectEndpoint.changeProjectStatusById(
                        new ProjectChangeStatusByIdRequest(userToken, testProjectName, null))
        );
        Assert.assertThrows(
                Exception.class, () -> projectEndpoint.changeProjectStatusById(
                        new ProjectChangeStatusByIdRequest(userToken, "wrongProject", Status.IN_PROGRESS))
        );
        @Nullable final ProjectCreateResponse projectCreateResponse = projectEndpoint.createProject(
                new ProjectCreateRequest(userToken, testProjectName, "Unit test project description")
        );
        Assert.assertNotNull(projectCreateResponse);
        Assert.assertNotNull(projectCreateResponse.getProject());
        @Nullable final ProjectChangeStatusByIdResponse response = projectEndpoint.changeProjectStatusById(
                new ProjectChangeStatusByIdRequest(userToken, projectCreateResponse.getProject().getId(), Status.IN_PROGRESS)
        );
        Assert.assertNotNull(response);
    }

    @Test
    public void projectClear() {
        Assert.assertThrows(Exception.class, () -> projectEndpoint.clearProject(
                new ProjectClearRequest())
        );
        Assert.assertThrows(Exception.class, () -> projectEndpoint.clearProject(
                new ProjectClearRequest(null))
        );
        Assert.assertThrows(Exception.class, () -> projectEndpoint.clearProject(
                new ProjectClearRequest("wrongToken"))
        );
        @Nullable ProjectClearResponse response = projectEndpoint.clearProject(
                new ProjectClearRequest(userToken)
        );
        Assert.assertNotNull(response);
    }

    @Test
    public void projectCreate() {
        @NotNull final String testProjectName = "UnitTestProject_" + UUID.randomUUID().toString();
        Assert.assertThrows(Exception.class, () -> projectEndpoint.createProject(
                new ProjectCreateRequest())
        );
        Assert.assertThrows(Exception.class, () -> projectEndpoint.createProject(
                new ProjectCreateRequest(null, null, null))
        );
        Assert.assertThrows(Exception.class, () -> projectEndpoint.createProject(
                new ProjectCreateRequest("wrongToken", "projectName", "projectDescription"))
        );
        @Nullable ProjectCreateResponse createResponse = projectEndpoint.createProject(
                new ProjectCreateRequest(userToken, testProjectName, "projectDescription")
        );
        Assert.assertNotNull(createResponse);
        Assert.assertNotNull(createResponse.getProject());
    }

    @Test
    public void projectGetById() {
        @NotNull final String testProjectName = "UnitTestProject_" + UUID.randomUUID().toString();
        Assert.assertThrows(Exception.class, () -> projectEndpoint.getProjectById(
                new ProjectGetByIdRequest()
        ));
        Assert.assertThrows(Exception.class, () -> projectEndpoint.getProjectById(
                new ProjectGetByIdRequest(null, null)
        ));
        Assert.assertThrows(Exception.class, () -> projectEndpoint.getProjectById(
                new ProjectGetByIdRequest("wrongToken", null)
        ));
        Assert.assertThrows(Exception.class, () -> projectEndpoint.getProjectById(
                new ProjectGetByIdRequest(userToken, null)
        ));
        @Nullable ProjectCreateResponse createResponse = projectEndpoint.createProject(
                new ProjectCreateRequest(userToken, testProjectName, "projectDescription")
        );
        Assert.assertNotNull(createResponse);
        Assert.assertNotNull(createResponse.getProject());
        @Nullable ProjectGetByIdResponse getResponse = projectEndpoint.getProjectById(
                new ProjectGetByIdRequest(userToken, createResponse.getProject().getId())
        );
        Assert.assertNotNull(getResponse);
        Assert.assertNotNull(getResponse.getProject());
    }

    @Test
    public void projectList() {
        @NotNull final String testProjectName = "UnitTestProject_" + UUID.randomUUID().toString();
        Assert.assertThrows(Exception.class, () -> projectEndpoint.listProject(
                new ProjectListRequest())
        );
        Assert.assertThrows(Exception.class, () -> projectEndpoint.listProject(
                new ProjectListRequest(null, null))
        );
        Assert.assertThrows(Exception.class, () -> projectEndpoint.listProject(
                new ProjectListRequest("", null))
        );
        Assert.assertThrows(Exception.class, () -> projectEndpoint.listProject(
                new ProjectListRequest("wrongToken", null))
        );
        @Nullable ProjectCreateResponse createResponse = projectEndpoint.createProject(
                new ProjectCreateRequest(userToken, testProjectName, "projectDescription")
        );
        Assert.assertNotNull(createResponse);
        Assert.assertNotNull(createResponse.getProject());
        @Nullable ProjectListResponse response = projectEndpoint.listProject(
                new ProjectListRequest(userToken, null)
        );
        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getProjects());
    }

    @Test
    public void projectRemoveById() {
        @NotNull final String testProjectName = "UnitTestProject_" + UUID.randomUUID().toString();
        Assert.assertThrows(Exception.class, () -> projectEndpoint.removeById(
                new ProjectRemoveByIdRequest())
        );
        Assert.assertThrows(Exception.class, () -> projectEndpoint.removeById(
                new ProjectRemoveByIdRequest(null, null)
        ));
        Assert.assertThrows(Exception.class, () -> projectEndpoint.removeById(
                new ProjectRemoveByIdRequest("", null)
        ));
        Assert.assertThrows(Exception.class, () -> projectEndpoint.removeById(
                new ProjectRemoveByIdRequest("wrongToken", null)
        ));
        Assert.assertThrows(Exception.class, () -> projectEndpoint.removeById(
                new ProjectRemoveByIdRequest(userToken, null)
        ));
        @Nullable ProjectCreateResponse createResponse = projectEndpoint.createProject(
                new ProjectCreateRequest(userToken, testProjectName, "projectDescription")
        );
        Assert.assertNotNull(createResponse);
        Assert.assertNotNull(createResponse.getProject());
        @Nullable ProjectRemoveByIdResponse response =
                projectEndpoint.removeById(new ProjectRemoveByIdRequest(userToken, createResponse.getProject().getId()));
        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getProject());
        Assert.assertThrows(Exception.class, () -> projectEndpoint.getProjectById(new ProjectGetByIdRequest(
                userToken,
                response.getProject().getId()))
        );
    }

    @Test
    public void projectUpdateById() {
        @NotNull final String testProjectName = "UnitTestProject_" + UUID.randomUUID().toString();
        Assert.assertThrows(Exception.class, () -> projectEndpoint.updateById(
                new ProjectUpdateByIdRequest(null, null, null, null)
        ));
        Assert.assertThrows(Exception.class, () -> projectEndpoint.updateById(
                new ProjectUpdateByIdRequest("wrongToken", null, null, null)
        ));
        Assert.assertThrows(Exception.class, () -> projectEndpoint.updateById(
                new ProjectUpdateByIdRequest(userToken, "wrongProjectId", null, null)
        ));
        @Nullable ProjectCreateResponse createResponse = projectEndpoint.createProject(
                new ProjectCreateRequest(userToken, testProjectName, "projectDescription")
        );
        Assert.assertNotNull(createResponse);
        Assert.assertNotNull(createResponse.getProject());
        @Nullable ProjectUpdateByIdResponse updateResponse = projectEndpoint.updateById(
                new ProjectUpdateByIdRequest(userToken, createResponse.getProject().getId(), "newName", "newDescription")
        );
        Assert.assertNotNull(updateResponse);
        Assert.assertNotNull(updateResponse.getProject());
        Assert.assertNotEquals(createResponse.getProject().getName(), updateResponse.getProject().getName());
        Assert.assertNotEquals(createResponse.getProject().getDescription(), updateResponse.getProject().getDescription());
        Assert.assertEquals(createResponse.getProject().getId(), updateResponse.getProject().getId());
    }

}
